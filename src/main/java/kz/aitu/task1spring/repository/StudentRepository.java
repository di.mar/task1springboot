package kz.aitu.task1spring.repository;

import kz.aitu.task1spring.model.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {
    List<Student> findAllByGroupId(int groupid);
}
