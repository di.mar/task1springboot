package kz.aitu.task1spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task1springApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task1springApplication.class, args);
	}

}
