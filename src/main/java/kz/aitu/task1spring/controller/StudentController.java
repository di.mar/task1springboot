package kz.aitu.task1spring.controller;


import kz.aitu.task1spring.repository.StudentRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {

    private final StudentRepository studentRepository;

    public StudentController(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @GetMapping("/api/students")
    public ResponseEntity<?> getStudents(){
        return ResponseEntity.ok(studentRepository.findAll());
    }

    @GetMapping("/api/students/{groupid}")
    public ResponseEntity<?> getStudentsByGroupId(@PathVariable int groupid){
        return ResponseEntity.ok(studentRepository.findAllByGroupId(groupid));
    }

}
